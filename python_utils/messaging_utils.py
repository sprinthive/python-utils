"""
  Copyright (c) 2018 SprintHive (Pty) Ltd (buzz@sprinthive.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
"""

import os
import logging

log = logging.getLogger(__name__)


def get_rabbitmq_credential(config, env_var, name):
    if os.getenv(env_var):
        log.debug('Got RabbitMQ %s from env var' % name)
        return os.getenv(env_var)
    else:
        if name in config['rabbitmq']:
            log.debug('Got RabbitMQ %s from config' % name)
            return config['rabbitmq'][name]
        else:
            raise Exception('Could not find RabbitMQ %s' % name)
