import os
import uuid
import logging.config

from .constants import confirmation_truth_words

logger = logging.getLogger(__name__)


# ----------------------------------------------------------------------------------------

def load_key(api, keys_dir):
    """
    Load and API key from file
    """

    logger.debug("keys dir: %s" % keys_dir)

    full_keys_dir = os.path.join(keys_dir, api)
    keys_file = 'api_key.txt'
    api_key = open(os.path.join(full_keys_dir, keys_file), 'r').readlines()[0].strip()

    logger.debug("loaded key for %s api" % api)

    return api_key


# ----------------------------------------------------------------------------------------

def is_true(parameter):
    """

    :param parameter:
    :return:
    """
    return parameter in confirmation_truth_words

# ----------------------------------------------------------------------------------------


def create_uuid():
    """

    :return:
    """
    logger.debug('Creating uuid...')
    return str(uuid.uuid4())

# ----------------------------------------------------------------------------------------
