import os
import logging
import yaml

log = logging.getLogger(__name__)


def load_yaml_config(path="../config/application.yaml"):
    config_local = '../config/application-local.yaml'
    config_file = config_local if os.path.exists(config_local) else path

    with open(config_file, 'rt') as f:
        config = yaml.safe_load(f.read())
    log.info('Loaded config file: %s' % config_file)

    return config
