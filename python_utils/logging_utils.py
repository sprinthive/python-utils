"""
  Copyright (c) 2018 SprintHive (Pty) Ltd (buzz@sprinthive.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
"""

import os
import logging
import logging.config
import yaml

log = logging.getLogger(__name__)


def setup_logging(
        default_path='../config/logging.yaml',
        default_level=logging.INFO,
        default_format='%(levelname)-7.7s] [%(asctime)s] [%(funcName)20s()] L%(lineno)d %(message)s',
        env_key='LOG_CFG'
):
    """
    Set up logging configuration
    """
    env_path = os.getenv(env_key, None)
    local_path = '../config/logging-local.yaml'

    if env_path is not None and os.path.exists(env_path):
        path = env_path
    else:
        if os.path.exists(local_path):
            path = local_path
        else:
            if os.path.exists(default_path):
                path = default_path
            else:
                logging.basicConfig(level=default_level, format=default_format)
                log.error("Can't find logging config file")
                raise Exception("Can't find logging config file")

    with open(path, 'rt') as f:
        config = yaml.safe_load(f.read())
    logging.config.dictConfig(config)
    log.info('Logging configured from file: %s' % path)


def setup_basic_logging():
    """
    Set up basic logging configuration
    """
    logging.basicConfig(format="%(levelname)-7.7s] [%(asctime)s] [%(funcName)10s()] L%(lineno)d %(message)s", level=logging.INFO)

# ----------------------------------------------------------------------------------------


def test_logging():
    """
    :return:
    """

    logging.info('Logging enabled in %s' % __name__)
    logging.info('TEST: INFO')
    logging.warning('TEST: WARNING')
    logging.error('TEST: ERROR')
    logging.debug('TEST: DEBUG')
