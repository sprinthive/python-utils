"""
  Copyright (c) 2018 SprintHive (Pty) Ltd (buzz@sprinthive.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
"""

import logging
import os


class LogContextFilter(logging.Filter):
    """
    Injects contextual information into the log such as appName, environment.namespace and environment.podName
    """

    def __init__(self):
        self.name = os.getenv('APP_NAME', 'unknown')
        self.podName = os.getenv('APP_POD_NAME', 'unknown')
        self.version = os.getenv('APP_VERSION', 'unknown')
        self.namespace = os.getenv('APP_NAMESPACE', 'unknown')

    def filter(self, record):
        record.appName = self.name
        record.version = self.version
        record.environment = {'podName': self.podName, 'namespace': self.namespace}

        return True
