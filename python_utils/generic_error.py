"""
  Copyright (c) 2018 SprintHive (Pty) Ltd (buzz@sprinthive.com)

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
"""

from datetime import datetime


class FieldError:
    def __init__(self, field, error_code):
        self.field = field
        self.error_code = error_code

    def to_dict(self):
        return {'field': self.field, 'errorCode': self.error_code}


class GenericError(Exception):
    status_code = 400

    # message, errors, and payload are kept in the signature for backwards compatibility but is otherwise ignored
    def __init__(self, message=None, status_code=None, payload=None, errorType='internal-server-error', errors=None,
                 field_errors=[], traceId=None):
        Exception.__init__(self)
        if status_code is not None:
            self.status_code = status_code
        self.errorType = errorType
        self.traceId = traceId
        self.field_errors = field_errors

    def to_dict(self):
        error_dict = {
            'httpCode': self.status_code,
            'errorType': self.errorType,
            'traceId': self.traceId,
            'timestamp': datetime.utcnow().isoformat() + "Z"
        }
        if len(self.field_errors):
            error_dict['errors'] = [field_error.to_dict() for field_error in self.field_errors]

        return {'error': error_dict}
