from setuptools import setup

setup(name='python_utils',
      version='v0.2.2',
      description='Python utils useful across projects',
      url='https://bitbucket.org/sprinthive/python_utils',
      author='SprintHive',
      author_email='buzz@sprinthive.com',
      license='Apache2',
      packages=['python_utils'],
      zip_safe=False)
