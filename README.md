# Python Utils

Python utils useful across projects. Versions are tagged manually.

Note the repo name of **python-utils** and the python-compatible package name of **python_utils**.

## Create A New Version

- Make your changes to this project and the dependent project
- Update setup.py with the new version number, e.g. **v0.0.1**
- Test that the dependent project works with the new version:
  - Remove old version: `pip uninstall python_utils`
  - In python-utils, install new version locally: `pip install .`
  - Run and test your dependent project
  - Remove local version: `pip uninstall python_utils`
- In this project:
  - Commit and push
  - Run `git tag -a <new version> -m '<version description>'`
  - Run `git push --tags`
  - See the following section

## Use An Existing Version

- Add/update the following in your app/requirements.txt: `git+ssh://git@bitbucket.org/sprinthive/python-utils.git@<version>`
- Run `pip install -r requirements.txt`
- Import: `from python_utils.<some package> import <some function>`